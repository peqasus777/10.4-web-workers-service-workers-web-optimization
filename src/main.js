import JoinUsSection from "./join-us-section.js";
import "./styles/style.css";
import "./styles/normalize.css";

document.addEventListener("DOMContentLoaded", async function () {
	const joinSection = JoinUsSection.createStandardSection();
	document.body.appendChild(joinSection.section);

	const heartSection = document.getElementById("heart-section");
	heartSection.insertAdjacentElement("afterend", joinSection.section);

	const cardsContainer = document.querySelector(".app-section_cards");
	try {
		const fetchStart = performance.now(); // Starting Time 
		const fetchResponse = await fetch("http://localhost:3000/community");
		const userData = await fetchResponse.json();
		const fetchEnd =  performance.now(); // Finishin Time 

		userData.forEach(user => {
			const card = document.createElement("div");
			card.classList.add("app-section_cards_card");

			card.innerHTML = `
        <img class="app-section_cards_card--img" src="${user.avatar}" alt="" />
        <p class="app-section_cards_card--p">
        Aliquip ex ea commodo consequat. Duis aute irure dolor in
        reprehenderit in voluptate velit esse cillum dolore eu fugiat
        nulla pariatur.
        </p>
        <author class="app-section_cards_card--aut">${user.firstName} ${user.lastName}</author>
        <p class="app-section_cards_card--info">
          ${user.position}
        </p>
      `;

			cardsContainer.appendChild(card);
		});

		// Measure page load speed
		const pageLoadTime = fetchEnd - fetchStart;
		// console.log("Page Load Time:", pageLoadTime, "ms");

		// Measure memory usage
		const memoryInfo = window.performance.memory;
		// console.log("Memory Usage:", memoryInfo.usedJSHeapSize / 1024 / 1024, "MB");

		const metrics = {
			pageLoadTime: pageLoadTime,
			memoryUsage: memoryInfo.usedJSHeapSize / 1024 / 1024,
		};
		console.log("Memory Usage:", memoryInfo.usedJSHeapSize / 1024 / 1024, "MB", "Page Load Time:", pageLoadTime, "ms");

		try {
			const response = await fetch("http://localhost:3000/analytics/performance", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(metrics),
			});
			const result = await response.json();
			console.log("Performance Metrics Sent:", result);
		} catch (error) {
			console.error("Error sending performance metrics:", error);
		}
	} catch (error) {
		console.error("Error fetching data:", error);
	}

	const worker = new Worker(new URL("./web-worker.js", import.meta.url));
	const btn = document.querySelectorAll("button");
	const inp = document.querySelector("input[type='email']");
	const inpBtn = [inp, ...btn];

	inpBtn.forEach(element => {
		element.addEventListener("click", () => {
			const clickEventData = {
				timestamp: Date.now(),
				element: element.tagName,
			};

			worker.postMessage({
				type: "click",
				data: clickEventData,
			});
		});
	});

	try {
		const response = await fetch("http://localhost:3000/analytics/user");
		const userData = await response.json();
		console.log("User Analytics Data:", userData);
	} catch (error) {
		console.error("Error fetching user analytics data:", error);
	}
});
