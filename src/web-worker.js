/* eslint-disable */
const clickDataBatch = [];

self.addEventListener("message", event => {
  const { type, data } = event.data;
  if (type === "click") {
    const clickEvent = {
      timestamp: data.timestamp,
      element: data.element,
    };
    
    clickDataBatch.push(clickEvent);

    // if (clickDataBatch.length <= 5) {
    //   sendBatchToServer(clickDataBatch);
    //   clickDataBatch.length = 0;
    // } 
    if (clickDataBatch.length  === 5 ) {
      sendBatchToServer(clickDataBatch);
      clickDataBatch.splice(0, clickDataBatch.length);
    }

  }
});

async function sendBatchToServer(clickData) {
  try {const userAnalytics = await fetch("http://localhost:3000/analytics/user", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    
      body: JSON.stringify(clickData),
    });

    if (userAnalytics.ok) { // Checking if the response is successful
      console.log("Click data batch sent successfully");
    } else {
      console.error("Error sending click data batch:", userAnalytics.statusText);
    }
  } catch (error) {
    console.error("Error sending click data batch:", error);
  }
}
